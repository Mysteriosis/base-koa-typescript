# Base Koa 2 Typescript Boilerplate
Quick Koa 2 boilerplate for little web projects

## Usage
- `yarn seed` : Execute a seeder file to populate the database
- `yarn eslint` : Execute ESLINT with Prettier
- `yarn test` : Execute Jest
- `yarn build` : Build project with `tsc`
- `yarn build-watch` : Build project continuously (with watcher)
- `yarn dev` : Execute dist folder with `nodemon`

## Dependencies
- dotenv
- eslint
- jest
- koa
- moment
- mongoose
- prettier
- pug
- typescript
- ...plus few other plugins to makes them work together

## Disclamer
This project has been created to offers me the basic structure I couldn't find in others Koa 2 boilerplates.  
This project neither pretends to be a reference, nor to respect all the good practices of this kind of application.