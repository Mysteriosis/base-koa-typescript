import Jwt from 'koa-jwt'
import config from '../config/config'

export default Jwt({
  secret: config.jwt.secret
})
