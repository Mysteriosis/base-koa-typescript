import User from '../../models/User'

test('Model User can save itself in database', () => {
  expect(typeof new User().save).toBe('function')
})
