import assert from 'assert'

import User from '../../models/User'

// DATA
const users = [
  {
    _id: '5c65ed0b3580a510ccc7e005',
    roles: ['admin', 'user'],
    email: 'pacurty@gmail.com',
    username: 'admin',
    password: 'test'
  }
]

// SAVE
const saves = [
  User.insertMany(users, { rawResult: true }, function (err, r) {
    assert.strictEqual(null, err)
    assert.strictEqual(users.length, r.insertedCount)
    User.db.close()
  })
]

Promise.all(saves).then(() => process.exit())
