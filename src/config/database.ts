import mongoose from 'mongoose'

import conf from './config'

// Connect to DB using the credentials set in the ".env" file
mongoose.connect(`mongodb://${conf.db.host}:${conf.db.port}/${conf.db.name}`, {
  // user: conf.db.user,
  // pass: conf.db.pass,
  useNewUrlParser: true
})

mongoose.Promise = Promise

export default mongoose
