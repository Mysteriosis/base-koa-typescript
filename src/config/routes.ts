import Router from 'koa-router'

// Controllers
import HomeController from '../controllers/homeController'

const router = new Router()
const homeController = new HomeController()

/**
 * Home page : Display home page
 * @return ./views/home.pug
 */
router.get('/', async (ctx) => {
  await homeController.index(ctx)
})

export default router
