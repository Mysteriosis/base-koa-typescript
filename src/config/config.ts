import dotenv from 'dotenv'
dotenv.config()

const config = {
  app: {
    port: process.env.LISTEN_PORT || 3000,
    env: process.env.ENV || 'development'
  },
  auth: {
    accessTokenTtl: 60 * 5,
    refreshTokenTtl: 60
  },
  db: {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || '27017',
    name: process.env.DB_NAME || 'test',
    user: process.env.DB_USER || 'root',
    pass: process.env.DB_PASSWORD || ''
  },
  jwt: {
    secret: process.env.JWT_SECRET || 'Black metal ist krieg'
  }
}

export default config
