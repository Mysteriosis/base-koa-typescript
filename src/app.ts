import Koa from 'koa'
import Pug from 'koa-pug'

import logger from 'koa-logger'
import bodyParser from 'koa-bodyparser'
import momentjs from 'moment'

import config from './config/config'
import router from './config/routes'

const app = new Koa()

// Init middlewares
app.use(logger())
app.use(bodyParser())
app.use(router.routes())

new Pug({
  viewPath: './src/views',
  basedir: './src/views',
  noCache: config.app.env === 'development',
  locals: {
    moment: momentjs
  },
  app: app
})

// Error handler in production
app.use(async (ctx, next) => {
  if (config.app.env === 'production') {
    try {
      await next()
      if ((ctx.status || 404) === 404) {
        ctx.throw(404)
      }
    } catch (err) {
      ctx.status = err.status || 500
      ctx.body = err.message
      ctx.app.emit('error', err, ctx)
    }
  }
})

// Start server
app.listen(config.app.port)
