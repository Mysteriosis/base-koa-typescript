import mongoose from 'mongoose'

export default class Controller {
  model: mongoose.Schema

  constructor(model: mongoose.Schema = null) {
    this.model = model !== undefined ? model : null
  }

  async index(): Promise<any> {
    return new Promise<any>((resolve) =>
      resolve(this.model ? this.model.find({}) : [])
    )
  }

  create() {}
  save() {}
  delete() {}
}
