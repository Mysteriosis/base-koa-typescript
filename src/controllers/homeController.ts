import Controller from './controller'

class homeController extends Controller {
  async index(ctx): Promise<any> {
    return ctx
      .render('home.pug', {
        title: 'Home page'
      })
      .catch((err) => console.log(err))
  }
}

export default homeController
